public class BikeStore {
    // Adam Atamnia 2036819
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("toyota", 6, 200);
        bicycles[1] = new Bicycle("brocar", 4, 400);
        bicycles[2] = new Bicycle("okcar", 7, 100);
        bicycles[3] = new Bicycle("honda", 2, 1500);

        for(int i = 0; i < bicycles.length; i++){
            System.out.println(bicycles[i].toString());
        }
    }
    
}
