// Adam Atamnia 2036819
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", MaxSpeed: " +this.maxSpeed;
    }
}